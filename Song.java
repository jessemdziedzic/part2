/** *************************************************************************
 * Revision History (newest first)
 ***************************************************************************
 * 01-27-2019 - Jesse Dziedzic - CmpArtist class and compare method begun
 * 01-17-2019 - Jesse Dziedzic - Constructor and Accessors completed
 * 2016 Anne Applin formatting and JavaDoc added
 * 2015 Starting code by Prof. Boothe
 ************************************************************************* */
package student;

import java.util.*;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * 01-18-2020 Revised by Suwigya Chaudhary Song class to hold strings for a
 * song's artist, title, and lyrics Do not add any methods, just implement the
 * ones that are here.
 *
 * @author boothe
 */
public class Song implements Comparable<Song> {

    //String artist;
    private String artist;//string representing the artist of the song
    // I added private access modifier so that only this class can access this property.

    //String title;
    private String title;//string representing the title of the song
    // I added private access modifier so that only this class can access this property.

    //String lyrics;
    private String lyrics;//the string containing the song's lyrics
    // I added private access modifier so that only this class can access this property.

    /**
     * Parameterized constructor
     *
     * @param artist the author of the song
     * @param title the title of the song
     * @param lyrics the lyrics as a string with linefeeds embedded
     */
    public Song(String artist, String title, String lyrics) {
        this.artist = artist;
        this.title = title;
        this.lyrics = lyrics;
    }

    /**
     * Class for comparing the artist of two different song objects, returning
     * with their relative position in natural order.
     */
    public static class CmpArtist extends CmpCnt implements Comparator<Song> {

        /**
         * A comparison method that returns such that the relative difference 
         * in natural ordering of an artist of a song (s1) compared to the 
         * artist of another song (s2) is shown as an integer value.
         * @param s1 The first song whose artist is to be compared to
         * @param s2 the second song, whose artist is compared against.
         * @return An integer representing their relative positions in natural
         * ordering (s1 to s2).
         */
        @Override
        public int compare(Song s1, Song s2) {
            cmpCnt++;
            return s1.getArtist().compareToIgnoreCase(s2.getArtist());
        }
    }

    /**
     * Accessor for the song's artist.
     *
     * @return The string representing the song's artist
     */
    public String getArtist() {
        return this.artist;
    }

    /**
     * Accessor for the song's lyrics.
     *
     * @return The lyrics as a string
     */
    public String getLyrics() {
        return this.lyrics;
    }

    /**
     * Accessor for the song's title.
     *
     * @return The string representing the song's title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Artist and Title only.
     *
     * @return a formatted string with artist and title
     */
    @Override
    public String toString() {
        return String.format("%s, \"%s\"", getArtist(), getTitle());
    }

    /**
     * the default comparison of songs primary key: artist, secondary key: title
     * used for sorting and searching the song array if two songs have the same
     * artist and title they are considered the same
     *
     * @param song2
     * @return a negative number, positive number or 0 depending on whether this
     * song should be before, after or is the same. Used for a "natural" sorting
     * order. In this case first by author then by title so that the all of an
     * artist's songs are together, but in alpha order. Follow the given
     * example.
     */
    @Override
    public int compareTo(Song song2) {
        // some constants for clarity
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;
        // shouldn't be any null objects, but if there are
        // put them at the end
        if (song2 == null) {
            return AFTER;
        }
        //if the addresses are the same... they are equal
        if (this == song2) {
            return EQUAL;
        }
        // sorting by artist's name 
        // this uses the String compareToIgnoreCase() method
        int comparison = this.getArtist().compareToIgnoreCase(((Song) song2).getArtist());
        if (comparison != EQUAL) {
            return comparison;
        }
        // if they have the same artist, we will sort by title name within 
        // that.
        comparison = this.getTitle().compareToIgnoreCase(((Song) song2).getTitle());
        if (comparison != EQUAL) {
            return comparison;
        }
        // if we get here we are problably looking at duplicate objects
        return EQUAL;
    }

    /**
     * testing method to unit test this class
     *
     * @param args
     */
    public static void main(String[] args) {
        Song s1 = new Song("Professor B",
                "Small Steps",
                "Write your programs in small steps\n"
                + "small steps, small steps\n"
                + "Write your programs in small steps\n"
                + "Test and debug every step of the way.\n");

        Song s2 = new Song("Brian Dill",
                "Ode to Bobby B",
                "Professor Bobby B., can't you see,\n"
                + "sometimes your data structures mystify me,\n"
                + "the biggest algorithm pro since Donald Knuth,\n"
                + "here he is, he's Robert Boothe!\n");

        Song s3 = new Song("Professor B",
                "Debugger Love",
                "I didn't used to like her\n"
                + "I stuck with what I knew\n"
                + "She was waiting there to help me,\n"
                + "but I always thought print would do\n\n"
                + "Debugger love .........\n"
                + "Now I'm so in love with you\n");

        CmpArtist artistCompare = new CmpArtist();

        System.out.println("testing getArtist: " + s1.getArtist());
        System.out.println("testing getTitle: " + s1.getTitle());
        System.out.println("testing getLyrics:\n" + s1.getLyrics());

        System.out.println("testing toString:\n");
        System.out.println("Song 1: " + s1);
        System.out.println("Song 2: " + s2);
        System.out.println("Song 3: " + s3);

        System.out.println("testing compareTo:");
        System.out.println("Song1 vs Song2 = " + s1.compareTo(s2));
        System.out.println("Song2 vs Song1 = " + s2.compareTo(s1));
        System.out.println("Song1 vs Song3 = " + s1.compareTo(s3));
        System.out.println("Song3 vs Song1 = " + s3.compareTo(s1));
        System.out.println("Song1 vs Song1 = " + s1.compareTo(s1));

        System.out.println("Testing Artist comparisons: ");
        System.out.println("Song 1 and Song 2: "
                + artistCompare.compare(s1, s2));
        System.out.println("Song 2 and Song 3: "
                + artistCompare.compare(s2, s3));
        System.out.println("Song 1 and Song 3: "
                + artistCompare.compare(s1, s3));
        System.out.println("Song 2 and Song 1: "
                + artistCompare.compare(s2, s1));
        System.out.println("Song 3 and Song 2: "
                + artistCompare.compare(s3, s2));
        System.out.println("Song 3 and Song 1: "
                + artistCompare.compare(s3, s1));
        System.out.println("Running comparison, songs against selves:"
                + System.lineSeparator() + artistCompare.compare(s1, s1)
                + System.lineSeparator() + artistCompare.compare(s2, s2)
                + System.lineSeparator() + artistCompare.compare(s3, s3));
    }
}