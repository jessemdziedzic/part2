/***************************************************************************
 * Revision History (newest first)
 ***************************************************************************
 * 
 * 2016 Anne Applin formatting and JavaDoc added 
 * 2015 Starting code by Prof. Boothe 
 **************************************************************************/

package student;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * A class that parses the data file and builds an array of Songs objects.
 * 
 * @author bboothe
 */
public class SongCollection {
    /**
     *  an array of Song objects 
     */
    private Song[] songs;

    /**
     * Note: in any other language, reading input inside a class is simply not
     * done!! No I/O inside classes because you would normally provide
     * precompiled classes and I/O is OS and Machine dependent and therefore
     * not portable. Java runs on a virtual machine that IS portable. 
     * So this is permissable because we are programming in Java.
     *
     * @param filename The path and filename to the datafile that we are using.
     */
    public SongCollection(String filename) {

	// use a try catch block
        // read in the song file and build the songs array
        // you must use a StringBuilder to read in the lyrics!
        StringBuilder forLyrics = new StringBuilder();
        ArrayList<Song> songsList = new ArrayList<>();
        char delimit = '\"';
        Scanner inFile = null;

        // use a try catch block
        // read in the song file and build the songs array
        // you must use a StringBuilder to read in the lyrics!
        try {
            inFile = new Scanner(new FileReader(filename));
            String artist = null;
            String title = null;
            String lyrics = null;
            if (!inFile.hasNext()) {
                System.out.println("The file is empty.");
                System.exit(0);
            }
            while (inFile.hasNextLine()) {
                String thisLine = inFile.nextLine();
                if (thisLine.startsWith("ARTIST")) {
                    int start = thisLine.indexOf(delimit);
                    int end = thisLine.lastIndexOf(delimit);
                    artist = thisLine.substring(start+1, end);
                    thisLine = inFile.nextLine();
                }
                if (thisLine.startsWith("TITLE")) {
                    int start = thisLine.indexOf(delimit);
                    int end = thisLine.lastIndexOf(delimit);
                    title = thisLine.substring(start+1, end);
                    thisLine = inFile.nextLine();
                } 
                if (thisLine.startsWith("LYRICS")) {
                    int start = thisLine.indexOf(delimit);
                    thisLine = thisLine.substring(start+1);
                    while (!thisLine.equals("\"")) {
                        forLyrics.append(thisLine).append(System.lineSeparator());
                        thisLine = inFile.nextLine();
                    } 
                    lyrics = forLyrics.toString();
                } 
                Song song = new Song(artist, title, lyrics);
                songsList.add(song);
                forLyrics.delete(0, forLyrics.length());
            }
            inFile.close();
        } catch (FileNotFoundException e) {
            System.err.println("Could not find the file: " + filename);
            return;
        } catch (NoSuchElementException e) {
            System.err.println("Could not read the file: " + filename);
            return;
        }

        // sort the songs array
        Collections.sort(songsList);
        Song[] a = new Song[songsList.size()];
        songs = songsList.toArray(a);
    }
 
    /**
     * this is used as the data source for building other data structures
     * @return the songs array
     */
    public Song[] getAllSongs() {
        return songs;
    }
 
    /**
     * unit testing method Start by setting shortSongs.txt as the argument
     * in the Project Properties.  
     * @param args
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("usage: prog songfile");
            return;
        }

        SongCollection sc = new SongCollection(args[0]);

        // show song count 
        int countSongs = 0;
        Song[] allSongs = sc.getAllSongs();
        for(int i = 0; i < allSongs.length; i++){
            if(allSongs[i] != null){
                countSongs++;
            }
        }
        System.out.println("Total songs = " + countSongs + ", first songs: ");
        //print first 10 songs(1 per line)
        Stream.of(sc.getAllSongs()).limit(10).forEach(System.out::println);
    }
}