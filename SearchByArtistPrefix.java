/** *************************************************************************
 * Revision History (newest first)
 ***************************************************************************
 * 01-31-2020 - Suwigya Chaudhary - added documentation.
 * 01-30-2020 Jesse Dziedzic Built Search method
 * 2016 Anne Applin formatting and JavaDoc added
 * 2015 Starting code by Prof. Boothe
 ************************************************************************* */
package student;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

/**
 * Search by Artist Prefix searches the artists in the song database for artists
 * that begin with the input String.
 *
 * @author bboothe
 */
public class SearchByArtistPrefix {

    // keep a direct reference to the song array
    private Song[] songs;

    /**
     * constructor initializes the property. {Done]
     *
     * @param sc a SongCollection object
     */
    public SearchByArtistPrefix(SongCollection sc) {
        songs = sc.getAllSongs();
    }

    /**
     * find all songs matching artist prefix, uses binary search, should operate
     * in time log n + k (# matches), converts artistPrefix to lowercase and
     * creates a Song object with artist prefix as the artist in order to have a
     * Song to compare. walks back to find the first "beginsWith" match, then
     * walks forward adding to the arrayList until it finds the last match.
     *
     * @param artistPrefix all or part of the artist's name
     * @return an array of songs by artists with substrings that match the
     * prefix
     */
    public Song[] search(String artistPrefix) {
        // changes the artistPrefix to lowercase
        String lowerPrefix = artistPrefix.toLowerCase();
        // arrayList for the search results
        ArrayList<Song> searchReturns = new ArrayList<>();
        // creating a song with that prefix for debugging
        Song search = new Song(lowerPrefix, null, null);
        // creating object of comparator
        Song.CmpArtist artistComp = new Song.CmpArtist();
        // calling Arrays.binarySearch with songs, search and artistComp
        int index = Arrays.binarySearch(songs, search, artistComp);
        // prints the number of times binary search was preformed.
        System.err.println("Binary search times: " + artistComp.cmpCnt);
        // reset the cmpCnt to 0
        artistComp.cmpCnt = 0;
        if (index < 0) {// if the index is negative change it to positive and substract 1.
            index = -index - 1;
        }
        
        boolean found = false;
        int backTrack = index - 1;
        while (!found) {
            // increases the compare count
            artistComp.cmpCnt++;
            if (backTrack >= 0) {
                while (songs[backTrack].getArtist().toLowerCase().startsWith(lowerPrefix)) {
                    // adds the song to the arrayList
                    searchReturns.add(songs[backTrack]);
                    // redeces the backtrack by 1
                    backTrack--;
                    // increases the compare count
                    artistComp.cmpCnt++;
                }
            }
            if (songs[index].getArtist().toLowerCase().startsWith(lowerPrefix)
                    && index < songs.length-1) {
                    // adds the song to the arrayList.
                searchReturns.add(songs[index]);
                    // increases the index
                    index++;
            } else {
                if (songs[index].getArtist().toLowerCase().startsWith(lowerPrefix)){
                    searchReturns.add(songs[index]);
                }
                // breaks the loop
                found = true;
            }
        }
        // sorts the arrayList
        Collections.sort(searchReturns);
        // initialises a array for the songs
        Song[] byArtistResult = new Song[searchReturns.size()];
        // creates the array from the arrayList
        byArtistResult = searchReturns.toArray(byArtistResult);
        // prints the compare count
        System.err.println("Search returns time: " + artistComp.cmpCnt);
        // reset the compare count to 0.
        artistComp.cmpCnt = 0;
        // returns the array.
        return byArtistResult;
    }

    /**
     * testing method for this unit
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("usage: prog songfile [search string]");
            return;
        }

        SongCollection sc = new SongCollection(args[0]);
        SearchByArtistPrefix sbap = new SearchByArtistPrefix(sc);

        if (args.length > 1) {
            System.out.println("searching for: " + args[1]);
            sbap.search(args[1]);
            Song[] byArtistResult = sbap.search(args[1]);
            // prints first ten songs
            System.out.println("First ten songs found by search: ");
            Stream.of(byArtistResult).limit(10).forEach(System.out::println);
        }
    }
}